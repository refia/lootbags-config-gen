window.addEventListener("load", function (event) {

    let bagNames = ['lootbagCommon', 'lootbagUncommon', 'lootbagRare', 'lootbagEpic', 'lootbagLegendary'];
    let bagColorNames = '$WHITE $DARK_BLUE $DARK_GREEN $DARK_AQUA $DARK_RED $DARK_PURPLE $GOLD $GRAY $DARK_GRAY $BLUE $GREEN $AQUA $RED $LIGHT_PURPLE $YELLOW';
    bagColorNames = bagColorNames.split(' ');
    let fieldsMap = {
        '$BAGTEXTOPENED': 'subTitle',
        '$BAGTEXTUNOPENED': 'subTitle',
        '$BAGTEXTCOLOR': 'textColor',
        '$MINIMUMITEMS': 'minItemsPerOpen',
        '$MAXIMUMITEMS': 'maxItemsPerOpen',
        '$BAGCOLOR': 'textureColor'
    };

    let bodyStaticParams = ['$ISSECRET:false', '$WEIGHT:0', '$CRAFTEDFROM:$NULL', '$PASSIVESPAWNWEIGHT:0',
        '$PLAYERSPAWNWEIGHT:0', '$MOBSPAWNWEIGHT:0', '$BOSSSPAWNWEIGHT:0', '$USEGENERALLOOTSOURCES:false',
        '$MAXIMUMGENERALLOOTWEIGHT:-1', '$MINIMUMGENERALLOOTWEIGHT:-1', '$PREVENTITEMREPEATS:damage', '$EXCLUDEENTITIES:true'];

    let app = new Vue({
        el: '#app',
        data: {
            rawData: null,
            configData: null,
            configPreviewParams: {buttons: []},
            configReaderParams: {buttons: []},
            showConfigModal: false,
            showConfigReaderModal: false,
            preventItemDuplicates: true,
            pageRendered: true,
            lang: 'en',
            simSpeed: 5,
            simLimit: 100,
            simTotal: 0,
            viewSim: false,
            isSimRunning: false,
            simData: [],
            showGeneralInfo: true,
            showJson: false,
            globalStyle: 'dark',
            showWarn: false,
            translateMap: translateMap,
            colorNames: bagColorNames,
            bags: defaultBagsData, /*defaultBagsData,*/
            bagNames: ['lootbagCommon', 'lootbagUncommon', 'lootbagRare', 'lootbagEpic', 'lootbagLegendary'],
            itemLabels: Object.keys(itemsDropNameToApi)
        },
        mounted: function(){
            let saveCnfBtn = {name: 'Save Generated Config', class: 'idle', action: ()=>{this.downloadConfig();}};
            let closeBtn = {name: 'close', action: ()=>{this.showConfigModal = false;}};
            this.configPreviewParams.buttons = [saveCnfBtn, closeBtn];
            this.bindUsedItems();
        },
        methods: {
            addBag: function () {
                this.bags.push({
                    name: 'lootbagCommon', subTitle: 'Bag subtitle...', textColor: '$WHITE', usedItemNames: [],
                    id: this.bags.length, minItemsPerOpen: 1, maxItemsPerOpen: 2, textureColor: '#ffffff',
                    items: [], includeDrop: [], isActive: this.bags.length === 0, listOfIncludedItems: [],
                    extendableBag: false
                });
            },
            bindUsedItems:function(){
                this.bags.forEach(bag => {
                    bag.usedItemNames = bag.usedItemNames || [];
                    bag.items.forEach(item => {
                        if(bag.usedItemNames.indexOf(item.name) < 0)
                            bag.usedItemNames.push(item.name);
                    });
                });
            },
            resetUsedItems: function(bag, key, ev){
                if(ev && ev.oldVal && bag.usedItemNames.indexOf(ev.oldVal) > -1) {
                    bag.usedItemNames.splice(bag.usedItemNames.indexOf(ev.oldVal), 1);
                }
                if(ev && ev.newVal && bag.usedItemNames.indexOf(ev.newVal) < 0) {
                    bag.usedItemNames.push(ev.newVal);
                }
            },
            readFile: function(ev){
                let file = ev.target.files[0];
                let reader = new FileReader();
                let name = file.name;
                reader.onload = (e) => {
                    let fileText = e.target.result;
                    window.tst = fileText;
                    let lines = fileText.split('\n');
                    let newBags = [];
                    let usedItems = [];
                    let isItemLines = false;
                    let unknownItems = [];
                    lines.forEach(line => {
                        line = line.trim();
                        let lineParts = line.split(':');
                        if(isSame(lineParts[0], '$STARTBAG')){
                            newBags.push({name: lineParts[1], id: lineParts[2], isActive: newBags.length === 0,
                                includeDrop: [], listOfIncludedItems: [], usedItemNames: [], items: [],
                                extendableBag: false});
                        }
                        if(isSame(lineParts[0], '$ENDWHITELIST')) isItemLines = false;
                        if(isItemLines) {
                            if(lineParts[0] && lineParts[1] && lineParts[2]){
                                let itemName = itemsDropApiToName[lineParts[0]+':'+lineParts[1]+':'+lineParts[2]];
                                if(itemName) {
                                    let itemData = {
                                        name: itemName,
                                        min: lineParts[3],
                                        max: lineParts[4],
                                        weight: lineParts[5]
                                    };
                                    newBags[newBags.length-1].items.push(itemData);
                                    if(newBags[newBags.length-1].usedItemNames.indexOf(itemName) < 0) {
                                        newBags[newBags.length-1].usedItemNames.push(itemName);
                                    }
                                    if(!usedItems[itemName]) {
                                        usedItems[itemName] = [{
                                            bagId: newBags[newBags.length-1].id,
                                            middleVal: ((Number(itemData.min) + Number(itemData.max)) / 2)
                                        }];
                                    } else {
                                        usedItems[itemName].push({
                                            bagId: newBags[newBags.length-1].id,
                                            middleVal: ((Number(itemData.min) + Number(itemData.max)) / 2)
                                        });
                                        usedItems[itemName].sort((a, b) => {
                                            if(a.middleVal > b.middleVal) return 1;
                                            if(a.middleVal < b.middleVal) return -1;
                                            return 0;
                                        });
                                    }
                                } else unknownItems.push(lineParts);
                            } else unknownItems.push(lineParts);
                        } else {
                            if(fieldsMap[lineParts[0]]){
                                if(lineParts[0] == '$BAGCOLOR'){
                                    console.log('$BAGCOLOR:', lineParts);
                                    newBags[newBags.length-1][fieldsMap[lineParts[0]]] = this.toHex(lineParts[1]);
                                    newBags[newBags.length-1].isCustom = true;
                                } else {
                                    newBags[newBags.length-1][fieldsMap[lineParts[0]]] = lineParts[1];
                                }
                            }
                        }
                        if(isSame(lineParts[0], '$STARTWHITELIST')) isItemLines = true;
                        //newBags
                    });
                    if(this.preventItemDuplicates) {
                        let bagsMap = {};
                        newBags.forEach(b=>bagsMap[b.id]=b);
                        let bagLookupsMap = {};
                        let itemNames = Object.keys(usedItems);
                        itemNames.forEach(iName => {
                            if(usedItems[iName].length > 1){
                                usedItems[iName].forEach((uItem, idx) => {
                                    if(idx > 0) {
                                        bagLookupsMap['bag:'+uItem.bagId] = bagLookupsMap['bag:'+uItem.bagId] || {};
                                        bagLookupsMap['bag:'+uItem.bagId]['to:'+usedItems[iName][0].bagId] =
                                            bagLookupsMap['bag:'+uItem.bagId]['to:'+usedItems[iName][0].bagId] || {mtp:[]};
                                        bagLookupsMap['bag:'+uItem.bagId]['to:'+usedItems[iName][0].bagId].mtp.push(
                                            uItem.middleVal / usedItems[iName][0].middleVal
                                        );
                                    }
                                });
                            }
                        });
                        let lupKeys = Object.keys(bagLookupsMap);
                        lupKeys.forEach(lk => {
                            let currentBag = bagsMap[lk.split(':')[1]];
                            let toKeys = Object.keys(bagLookupsMap[lk]);
                            toKeys.forEach(tk => {
                                let targetBag = bagsMap[tk.split(':')[1]];
                                let mtpCount = bagLookupsMap[lk][tk].mtp.length;
                                let mtpSum = 0;
                                bagLookupsMap[lk][tk].mtp.forEach(num => {
                                    mtpSum += num;
                                }, 0);
                                let middleMtp = Math.round(mtpSum / mtpCount);
                                currentBag.extendableBag = true;
                                currentBag.includeDrop.push({
                                    increaseDropItems: middleMtp,
                                    reduceDropWeight: middleMtp,
                                    bagId: targetBag.id
                                });
                            });
                        });
                        this.bags = newBags;
                        this.bags.forEach(b=>{
                            this.recalcDrop(b);
                        });
                        this.bags = this.resetLocalWeight(this.bags);
                    }
                    if(unknownItems.length > 0){
                        alert('unknownItems: ', unknownItems);
                        console.error('unknownItems: ', unknownItems);
                    }
                };
                reader.readAsText(file);
            },
            resetLocalWeight: function(bags){
                bags.forEach(bag=>{
                    let minWeight = null;
                    bag.items.forEach(item => {
                        if(minWeight === null || Number(minWeight) > Number(item.weight)) minWeight = Number(item.weight);
                    });
                    bag.items.forEach(item => {
                        item.weight = Number(item.weight) / minWeight;
                    });
                });
                return bags;
            },
            toHex: function(str){
                str = str.split('|');
                return rgbToHex(Number(str[0]), Number(str[1]), Number(str[2]));
            },
            stopSim: function(){
                Vue.nextTick(()=>{
                    this.isSimRunning = false;
                });
            },
            showReaderModal: function(){
                this.showConfigReaderModal = true;
                let closeBtn = {name: 'close', action: ()=>{this.showConfigReaderModal = false;}};
                this.configReaderParams = {
                    buttons: [closeBtn],
                    bodyStyle: {'text-align': 'center', 'max-width': '350px'}
                };
            },
            readConfigFromFile: function(){

            },
            rerenderPage: function(){
                this.pageRendered = false;
                setTimeout(() => {
                    Vue.nextTick(() => {
                        this.pageRendered = true;
                    });
                },50);
            },
            genAndShowConfig: function(){
                this.genConfig();
                this.showConfigModal = true;
            },
            genConfig: function(){
                this.configData = 'some data \nto test';
                let result = '';
                this.bags.forEach(bag => {
                    let bagTextConfig = `$STARTBAG:${bag.name}:${bag.id}\n`;
                    bodyStaticParams.forEach(bsp => {
                        bagTextConfig += `\t${bsp}\n`;
                    });
                    let configFieldNames = Object.keys(fieldsMap);
                    configFieldNames.forEach(cfn => {
                        if(fieldsMap[cfn] == 'textureColor') {
                            if(bag.isCustom){
                                let colorVal = hexToRgb(bag[fieldsMap[cfn]]).join('|');
                                bagTextConfig += '\t' + cfn + ':' + colorVal + ':' + colorVal + '\n';
                            }
                        } else {
                            bagTextConfig += '\t' + cfn + ':' + bag[fieldsMap[cfn]] + '\n';
                        }
                    });
                    bagTextConfig += '\t$STARTWHITELIST' + '\n';
                    bagTextConfig += this.buildItemsDropList(bag);
                    bagTextConfig += '\t$ENDWHITELIST' + '\n';
                    bagTextConfig += '$ENDBAG:' + bag.name + '\n';
                    result += bagTextConfig;
                });
                this.configData = result;
            },
            buildItemsDropList: function(bag){
                let dropListText = '';
                bag.items.forEach(item => {
                    dropListText += `\t\t${itemsDropNameToApi[item.name]}:${item.min}:${item.max}:${item.globalWeight}\n`;
                });
                bag.listOfIncludedItems.forEach(item => {
                    let itemsMin = item.increaseDropItems * item.min;
                    if(itemsMin > 64) itemsMin = 64;
                    let itemsMax = item.increaseDropItems * item.max;
                    if(itemsMax > 64) itemsMax = 64;
                    dropListText += `\t\t${itemsDropNameToApi[item.name]}:${itemsMin}:${itemsMax}:${item.globalWeight}\n`;
                });
                return dropListText;
            },
            downloadConfig: function(){
                downloadAsFile(this.configData);
                this.configData = null;
                this.showConfigModal = false;
            },
            setStyle: function () {
                document.getElementsByTagName('html')[0].className = this.globalStyle;
            },
            addDropItem: function (bag) {
                bag.items.push({name: '', min: 1, max: 1, weight: 2, percent: '?'});
                this.recalcGlobPercentWeight(bag);
            },
            cancelExtending: function(bag){
                bag.includeDrop.splice(0, bag.includeDrop.length);
                this.recalcDrop(bag);
            },
            checkMultipliers: function (incData) {
                let fields = ['increaseDropItems', 'reduceDropWeight'];
                fields.forEach(fld => {
                    if (isNaN(incData[fld])) {
                        incData[fld] = 2
                    } else {
                        incData[fld] = Number(incData[fld]);
                        if (incData[fld] < 2) incData[fld] = 2;
                    }
                });
            },
            startSim: function(bag){
                this.simData = {bag: bag, opensCount: 0, items: []};
                this.viewSim = true;
                this.isSimRunning = true;
                this.simData.items = Object.assign([], bag.items.concat(bag.listOfIncludedItems));
                let totalWeight = 0;
                let itemsMap;
                let resetItemsMap = (isStart) => {
                    itemsMap = [];
                    this.simData.items.forEach(item =>
                    {
                        for (let i = 0; i < item.globalWeight; i++){
                            if(isStart){
                                item.meetSpotCount = 0;
                                item.countOfGotItemsTotal = 0;
                                item.itemGotCountFirstTry = 0;
                                item.itemsPerMeet = 0;
                                item.itemsPerOpen = 0;
                                item.simPercent = 0;
                                item.simPercentIfAlone = 0;
                            }
                            itemsMap.push(item);
                        }
                    });
                };
                let previousItemsInSameBag = [];
                let openBag = (itemsPerBag, isFirstOpen) => {
                    let item = itemsMap.splice(rand(0, itemsMap.length-1), 1)[0];
                    itemsMap.reduce((sum, mItem)=>{
                        if(mItem.name != item.name) sum.push(mItem);
                        return sum;
                    },[]);
                    item.meetSpotCount++;
                    let itemsMin, itemsMax;
                    if(item.increaseDropItems){
                        itemsMin = item.increaseDropItems * item.min;
                        if(itemsMin > 64) itemsMin = 64;
                        itemsMax = item.increaseDropItems * item.max;
                        if(itemsMax > 64) itemsMax = 64;
                    }
                    item.countOfGotItemsTotal += rand(itemsMin || item.min, itemsMax || item.max);
                    item.itemsPerMeet = toTwoDec(item.countOfGotItemsTotal / item.meetSpotCount);
                    item.itemsPerOpen = toTwoDec(item.countOfGotItemsTotal / (this.simData.opensCount + 1));
                    item.simPercent =  toTwoDec(item.meetSpotCount / (this.simData.opensCount + 1) * 100);
                    if(isFirstOpen) {
                        item.itemGotCountFirstTry++;
                        item.simPercentIfAlone = toTwoDec(item.itemGotCountFirstTry / (this.simData.opensCount + 1) * 100);
                    }

                    itemsPerBag--;
                    if(itemsPerBag == 0 || itemsMap.length == 0) {
                        this.simData.opensCount++;
                        setTimeout(()=>{
                            if(this.isSimRunning && this.simLimit > this.simData.opensCount){
                                resetItemsMap();
                                openBag(rand(bag.minItemsPerOpen, bag.maxItemsPerOpen), true);
                            } else {
                                this.isSimRunning = false;
                            }
                        }, 1000 / (isNaN(this.simSpeed) ? 1000 : this.simSpeed));
                    } else {
                        openBag(itemsPerBag, false);
                    }
                    //Vue.nextTick(()=>{});
                };
                resetItemsMap(true);
                openBag(rand(bag.minItemsPerOpen, bag.maxItemsPerOpen), false)
            },
            deleteBag: function(bag, bagIdx){
                if(!confirm('Do you really want to delete this bag?')) return;
                this.bags.forEach((bg, bIdx)=>{
                    let idxToDel;
                    bg.includeDrop.forEach((incpd, inIdx) => {
                        if(incpd.bagId > bagIdx) incpd.bagId--;
                        else if(incpd.bagId == bagIdx) idxToDel = inIdx;
                    });
                    if(idxToDel != null) bg.includeDrop.splice(idxToDel, 1);
                });
                this.bags.splice(bagIdx, 1);
                this.bags.forEach((bg, bIdx)=>{
                    bg.id = bIdx;
                });
                if(this.bags.length > 0){
                    this.bags[0].isActive = true;
                    this.recalcDrop(this.bags[0]);
                }
            },
            recalcDrop: function (bag) {
                bag.maxWeightReducer = 2;
                bag.listOfIncludedItems.splice(0, bag.listOfIncludedItems.length);
                bag.includeDrop.forEach(incData => {
                    if(bag.maxWeightReducer < incData.reduceDropWeight) {
                        bag.maxWeightReducer = incData.reduceDropWeight;
                    }
                });
                this.bags.forEach(bg => {
                    bag.includeDrop.forEach(incData => {
                        if (bg.id == incData.bagId) {
                            incData.listOfExtendedItems = [];
                            bg.items.forEach(otherBagItem => {
                                let incItem = Object.assign({}, otherBagItem);
                                bag.items.forEach((currentBagItem, idx) => {
                                    if(currentBagItem.name == otherBagItem.name){
                                        bag.items.splice(idx,1);
                                    }
                                });
                                incItem.isIncluded = true;
                                incItem.increaseDropItems = incData.increaseDropItems;
                                incItem.globalWeight = Math.round(incItem.weight * (bag.maxWeightReducer / incData.reduceDropWeight));
                                bag.listOfIncludedItems.push(incItem);
                            });
                        }
                    });
                });
                this.recalcGlobPercentWeight(bag);
            },
            getItemApiName: function (label) {
                return itemsDropNameToApi[label];
            },
            checkVisability: function (bag, bagToInc) {
                let isHidden = false;
                if (bag.id == bagToInc.id) {
                    isHidden = true;
                } else {
                    bag.includeDrop.forEach(inc => {
                        if (inc.bagId == bagToInc.id) isHidden = true;
                    });
                }
                return isHidden;
            },
            chooseBag: function (bag) {
                this.bags.forEach(b => {
                    b.isActive = false;
                });
                bag.isActive = true;
                this.recalcDrop(bag);
            },
            delItem: function (bag, idx) {
                this.resetUsedItems(bag, idx, {oldVal: bag.items[idx].name});
                bag.items.splice(idx, 1);
                this.recalcGlobPercentWeight(bag);
            },
            delIncDrop: function (bag, idx) {
                bag.includeDrop.splice(idx, 1);
                this.recalcDrop(bag);
            },
            recalcGlobPercentWeight: function (bag) {
                bag.maxWeightReducer = bag.includeDrop.length > 0 ? bag.maxWeightReducer : 1;
                let totalWeight = 0;
                bag.items.forEach(i => {
                    if (isNaN(i.weight)) {
                        i.weight = 2;
                    } else {
                        i.weight = Number(i.weight);
                        if (i.weight < 1) i.weight = 1;
                    }
                    i.globalWeight = i.weight * bag.maxWeightReducer;
                    totalWeight += i.globalWeight;
                });
                bag.listOfIncludedItems.forEach(i => {
                    if (isNaN(i.weight)) {
                        i.weight = 2;
                    } else {
                        i.weight = Number(i.weight);
                        if (i.weight < 1) i.weight = 1;
                    }
                    totalWeight += i.globalWeight;
                });
                bag.items.forEach(i => {
                    i.percent = toTwoDec((i.globalWeight / totalWeight) * 100);
                                    });
                bag.listOfIncludedItems.forEach(i => {
                    i.percent = toTwoDec((i.globalWeight / totalWeight) * 100);
                });
            },
            checkMinMax: function (item) {
                item.min = Number(item.min);
                item.max = Number(item.max);
                if (!item.min || isNaN(item.min) || item.min < 1) item.min = 1;
                if (!item.max || isNaN(item.max) || item.max > 64) item.max = 64;
                if (item.max < item.min) item.max = item.min;
            },
            checkMinMaxForBag: function (bag) {
                bag.minItemsPerOpen = Number(bag.minItemsPerOpen);
                bag.maxItemsPerOpen = Number(bag.maxItemsPerOpen);
                if (!bag.minItemsPerOpen || isNaN(bag.minItemsPerOpen) || bag.minItemsPerOpen < 1) bag.minItemsPerOpen = 1;
                if (!bag.maxItemsPerOpen || isNaN(bag.maxItemsPerOpen) || bag.maxItemsPerOpen > 5) bag.maxItemsPerOpen = 5;
                if (bag.maxItemsPerOpen < bag.minItemsPerOpen) bag.maxItemsPerOpen = bag.minItemsPerOpen;
            }
        }
    });

});