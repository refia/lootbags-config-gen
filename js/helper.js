
const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
    const hex = x.toString(16);
    return hex.length === 1 ? '0' + hex : hex
}).join('');

function isSame(str1, str2) {
    return String(str1).trim().toLowerCase() == String(str2).trim().toLowerCase();
}

function logLocally(text) {
    var loclog = document.getElementById('loclog');
    loclog.innerText += '\n' + text;
    loclog.scrollTop = loclog.scrollHeight;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)] : null;
}

function downloadAsFile(data, type) {
    let file = new Blob([data], {type: (type || 'data:text/plain;charset=utf-8')});
    let a = document.createElement("a");
    let url = URL.createObjectURL(file);
    a.href = url;
    a.download = 'Lootbags_BagConfig.cfg';
    a.text = 'get new config';
    document.body.appendChild(a);
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function toTwoDec(num) {
    return Math.round(num * 100) / 100;
}

function proc(finput) {
    window.finput = finput;
    fr = new FileReader();
    fr.onload = function (res) {
        console.log('loaded!');
        console.log(res.target.result);
        // createLink(JSON.stringify(window.bq), 'text');
    };
    fr.readAsText(finput.files[0]);
}

function rand(min, max) {
    min = Number(min);
    max = Number(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}