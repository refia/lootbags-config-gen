let defaultBagsData = [
  {
    "name": "lootbagCommon",
    "id": 0,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "10",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 4.17,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 4.17,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 4.17,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 4.17,
        "isIncluded": true,
        "increaseDropItems": 4
      }
    ],
    "usedItemNames": [
      "Galena Ore",
      "Pentlandite Ore",
      "Gold Ore",
      "Nether Quartz Ore",
      "Salt Ore",
      "Lepidolite Ore",
      "Coal Ore",
      "Bauxite Ore",
      "LV Circuit",
      "Tiny Pile of Gallium Dust",
      "ULV Machine Hull",
      "Blaze Powder",
      "Magnetite Ore",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Galena Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Pentlandite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Gold Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Nether Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Salt Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Lepidolite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Coal Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "Bauxite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 8.33
      },
      {
        "name": "LV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 4.17
      },
      {
        "name": "Tiny Pile of Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 4.17
      },
      {
        "name": "ULV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 4.17
      },
      {
        "name": "Blaze Powder",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 4.17
      }
    ],
    "extendableBag": true,
    "subTitle": "It may contain some common GT ores or GT items",
    "textColor": "$DARK_AQUA",
    "minItemsPerOpen": "2",
    "maxItemsPerOpen": "2",
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagUncommon",
    "id": 1,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": "2",
        "bagId": "0",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": "4",
        "reduceDropWeight": 2,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 2,
        "bagId": "10",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Galena Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Pentlandite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Gold Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Nether Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Salt Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lepidolite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Coal Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Bauxite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "LV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 1.19,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Tiny Pile of Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 1.19,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "ULV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 1.19,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Blaze Powder",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 1.19,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": "4"
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 2.38,
        "isIncluded": true,
        "increaseDropItems": 4
      }
    ],
    "usedItemNames": [
      "Stibnite Ore",
      "Cobaltite Ore",
      "Sapphire Ore",
      "Emerald Ore",
      "Diamond Ore",
      "Pitchblende Ore",
      "Sphalerite Ore",
      "Tantalite Ore",
      "Vanadium Magnetite Ore",
      "Bastnasite Ore",
      "Certus Quartz Ore",
      "LV Machine Hull",
      "MV Circuit",
      "HV Circuit",
      "Blaze Rod",
      "Glowstone Dust",
      "Small Pile of Gallium Gust",
      "Galena Ore",
      "Pentlandite Ore",
      "Gold Ore",
      "Nether Quartz Ore",
      "Salt Ore",
      "Lepidolite Ore",
      "Coal Ore",
      "Bauxite Ore",
      "LV Circuit",
      "Tiny Pile of Gallium Dust",
      "ULV Machine Hull",
      "Blaze Powder",
      "Magnetite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Stibnite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Cobaltite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Sapphire Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Emerald Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Diamond Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Pitchblende Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Sphalerite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Tantalite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Vanadium Magnetite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Bastnasite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "Certus Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 4.76
      },
      {
        "name": "LV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      },
      {
        "name": "MV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      },
      {
        "name": "HV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      },
      {
        "name": "Blaze Rod",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      },
      {
        "name": "Glowstone Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      },
      {
        "name": "Small Pile of Gallium Gust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 2.38
      }
    ],
    "extendableBag": true,
    "subTitle": "It may contain some uncommon ores or items",
    "textColor": "$DARK_GREEN",
    "minItemsPerOpen": "2",
    "maxItemsPerOpen": "3",
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagRare",
    "id": 2,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 4,
        "bagId": "0",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 8,
        "reduceDropWeight": 4,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 8,
        "reduceDropWeight": 4,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 8,
        "reduceDropWeight": 4,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 8,
        "reduceDropWeight": 4,
        "bagId": "10",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "1",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Galena Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Pentlandite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Gold Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Nether Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Salt Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Lepidolite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Coal Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Bauxite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "LV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.52,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Tiny Pile of Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.52,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "ULV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.52,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Blaze Powder",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.52,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Stibnite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Cobaltite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Sapphire Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Emerald Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Diamond Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Pitchblende Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Sphalerite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Tantalite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Vanadium Magnetite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Bastnasite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Certus Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 4,
        "percent": 2.08,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "LV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "MV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "HV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Blaze Rod",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Glowstone Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Small Pile of Gallium Gust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 1.04,
        "isIncluded": true,
        "increaseDropItems": 8
      }
    ],
    "usedItemNames": [
      "Scheelite Ore",
      "Sheldonite Ore",
      "Molybdenite Ore",
      "Tungstate Ore",
      "Titanium Dust",
      "Gallium Dust",
      "Stainless Steel Dust",
      "EV Circuit",
      "IV Circuit",
      "1k ME Storage Component",
      "1k ME Fluid Storage Component",
      "Glowstone",
      "Ender Pearl",
      "MV Machine Hull",
      "Galena Ore",
      "Pentlandite Ore",
      "Gold Ore",
      "Nether Quartz Ore",
      "Salt Ore",
      "Lepidolite Ore",
      "Coal Ore",
      "Bauxite Ore",
      "LV Circuit",
      "Tiny Pile of Gallium Dust",
      "ULV Machine Hull",
      "Blaze Powder",
      "Stibnite Ore",
      "Cobaltite Ore",
      "Sapphire Ore",
      "Emerald Ore",
      "Diamond Ore",
      "Pitchblende Ore",
      "Sphalerite Ore",
      "Tantalite Ore",
      "Vanadium Magnetite Ore",
      "Bastnasite Ore",
      "Certus Quartz Ore",
      "LV Machine Hull",
      "MV Circuit",
      "HV Circuit",
      "Blaze Rod",
      "Glowstone Dust",
      "Small Pile of Gallium Gust",
      "Magnetite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Scheelite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 12,
        "percent": 6.25
      },
      {
        "name": "Sheldonite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 12,
        "percent": 6.25
      },
      {
        "name": "Molybdenite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 12,
        "percent": 6.25
      },
      {
        "name": "Tungstate Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 12,
        "percent": 6.25
      },
      {
        "name": "Titanium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 4.17
      },
      {
        "name": "Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 4.17
      },
      {
        "name": "Stainless Steel Dust",
        "min": "2",
        "max": "3",
        "weight": 2,
        "globalWeight": 8,
        "percent": 4.17
      },
      {
        "name": "EV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 2.08
      },
      {
        "name": "IV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 2.08
      },
      {
        "name": "1k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 2.08
      },
      {
        "name": "1k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 2.08
      },
      {
        "name": "Glowstone",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 4.17
      },
      {
        "name": "Ender Pearl",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 4.17
      },
      {
        "name": "MV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 2.08
      }
    ],
    "extendableBag": true,
    "subTitle": "It may contain some rare ores or items",
    "textColor": "$BLUE",
    "minItemsPerOpen": "3",
    "maxItemsPerOpen": "3",
    "maxWeightReducer": 4
  },
  {
    "name": "lootbagEpic",
    "id": 3,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 6,
        "reduceDropWeight": 6,
        "bagId": "0",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 12,
        "reduceDropWeight": 6,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 12,
        "reduceDropWeight": 6,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 12,
        "reduceDropWeight": 6,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 12,
        "reduceDropWeight": 6,
        "bagId": "10",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 4,
        "bagId": "1",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "2",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Galena Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Pentlandite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Gold Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Nether Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Salt Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Lepidolite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Coal Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Bauxite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "LV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.45,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Tiny Pile of Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.45,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "ULV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.45,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Blaze Powder",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.45,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Stibnite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Cobaltite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Sapphire Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Emerald Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Diamond Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Pitchblende Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Sphalerite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Tantalite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Vanadium Magnetite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Bastnasite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Certus Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "LV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "MV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "HV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Blaze Rod",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Glowstone Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Small Pile of Gallium Gust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Scheelite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 9,
        "percent": 4.09,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Sheldonite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 9,
        "percent": 4.09,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Molybdenite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 9,
        "percent": 4.09,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Tungstate Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 9,
        "percent": 4.09,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Titanium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 2.73,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 2.73,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Stainless Steel Dust",
        "min": "2",
        "max": "3",
        "weight": 2,
        "globalWeight": 6,
        "percent": 2.73,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "EV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "IV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "1k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "1k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Glowstone",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 2.73,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Ender Pearl",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 2.73,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "MV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 3,
        "percent": 1.36,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 12
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 12
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 12
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.91,
        "isIncluded": true,
        "increaseDropItems": 12
      }
    ],
    "usedItemNames": [
      "Iridium Ore",
      "HV Machine Hull",
      "Blank Pattern",
      "4k ME Storage Component",
      "4k ME Fluid Storage Component",
      "ME Interface",
      "Arsenic Dust",
      "Lutetium Dust",
      "Galena Ore",
      "Pentlandite Ore",
      "Gold Ore",
      "Nether Quartz Ore",
      "Salt Ore",
      "Lepidolite Ore",
      "Coal Ore",
      "Bauxite Ore",
      "LV Circuit",
      "Tiny Pile of Gallium Dust",
      "ULV Machine Hull",
      "Blaze Powder",
      "Stibnite Ore",
      "Cobaltite Ore",
      "Sapphire Ore",
      "Emerald Ore",
      "Diamond Ore",
      "Pitchblende Ore",
      "Sphalerite Ore",
      "Tantalite Ore",
      "Vanadium Magnetite Ore",
      "Bastnasite Ore",
      "Certus Quartz Ore",
      "LV Machine Hull",
      "MV Circuit",
      "HV Circuit",
      "Blaze Rod",
      "Glowstone Dust",
      "Small Pile of Gallium Gust",
      "Scheelite Ore",
      "Sheldonite Ore",
      "Molybdenite Ore",
      "Tungstate Ore",
      "Titanium Dust",
      "Gallium Dust",
      "Stainless Steel Dust",
      "EV Circuit",
      "IV Circuit",
      "1k ME Storage Component",
      "1k ME Fluid Storage Component",
      "Glowstone",
      "Ender Pearl",
      "MV Machine Hull",
      "Magnetite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Iridium Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 12,
        "percent": 5.45
      },
      {
        "name": "HV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 6,
        "percent": 2.73
      },
      {
        "name": "Blank Pattern",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 6,
        "percent": 2.73
      },
      {
        "name": "4k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 6,
        "percent": 2.73
      },
      {
        "name": "4k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 6,
        "percent": 2.73
      },
      {
        "name": "ME Interface",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 6,
        "percent": 2.73
      },
      {
        "name": "Arsenic Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 12,
        "percent": 5.45
      },
      {
        "name": "Lutetium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 12,
        "percent": 5.45
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain more expensive ores or items",
    "textColor": "$DARK_PURPLE",
    "minItemsPerOpen": "4",
    "maxItemsPerOpen": "4",
    "maxWeightReducer": 6
  },
  {
    "name": "lootbagLegendary",
    "id": 4,
    "isActive": true,
    "includeDrop": [
      {
        "increaseDropItems": 8,
        "reduceDropWeight": 8,
        "bagId": "0",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 16,
        "reduceDropWeight": 8,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 16,
        "reduceDropWeight": 8,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 16,
        "reduceDropWeight": 8,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 16,
        "reduceDropWeight": 8,
        "bagId": "10",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 6,
        "reduceDropWeight": 6,
        "bagId": "1",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 4,
        "reduceDropWeight": 4,
        "bagId": "2",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "3",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Galena Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Pentlandite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Gold Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Nether Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Salt Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Lepidolite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Coal Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Bauxite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "LV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Tiny Pile of Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "ULV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Blaze Powder",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 8
      },
      {
        "name": "Stibnite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Cobaltite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Sapphire Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Emerald Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Diamond Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Pitchblende Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Sphalerite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Tantalite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Vanadium Magnetite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Bastnasite Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Certus Quartz Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 3,
        "percent": 1.22,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "LV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "MV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "HV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Blaze Rod",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Glowstone Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Small Pile of Gallium Gust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 1,
        "percent": 0.41,
        "isIncluded": true,
        "increaseDropItems": 6
      },
      {
        "name": "Scheelite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 6,
        "percent": 2.45,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Sheldonite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 6,
        "percent": 2.45,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Molybdenite Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 6,
        "percent": 2.45,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Tungstate Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 6,
        "percent": 2.45,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Titanium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Gallium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Stainless Steel Dust",
        "min": "2",
        "max": "3",
        "weight": 2,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "EV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "IV Circuit",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "1k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "1k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Glowstone",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Ender Pearl",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "MV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 4
      },
      {
        "name": "Iridium Ore",
        "min": "2",
        "max": "4",
        "weight": 2,
        "globalWeight": 8,
        "percent": 3.27,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "HV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Blank Pattern",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "4k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "4k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "ME Interface",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 4,
        "percent": 1.63,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Arsenic Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 3.27,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lutetium Dust",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 8,
        "percent": 3.27,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 16
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 16
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 16
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 0.82,
        "isIncluded": true,
        "increaseDropItems": 16
      }
    ],
    "usedItemNames": [
      "EV Machine Hull",
      "16k ME Storage Component",
      "16k ME Fluid Storage Component",
      "Naquadah Ore",
      "Osmium Dust",
      "Indium Dust",
      "Galena Ore",
      "Pentlandite Ore",
      "Gold Ore",
      "Nether Quartz Ore",
      "Salt Ore",
      "Lepidolite Ore",
      "Coal Ore",
      "Bauxite Ore",
      "LV Circuit",
      "Tiny Pile of Gallium Dust",
      "ULV Machine Hull",
      "Blaze Powder",
      "Stibnite Ore",
      "Cobaltite Ore",
      "Sapphire Ore",
      "Emerald Ore",
      "Diamond Ore",
      "Pitchblende Ore",
      "Sphalerite Ore",
      "Tantalite Ore",
      "Vanadium Magnetite Ore",
      "Bastnasite Ore",
      "Certus Quartz Ore",
      "LV Machine Hull",
      "MV Circuit",
      "HV Circuit",
      "Blaze Rod",
      "Glowstone Dust",
      "Small Pile of Gallium Gust",
      "Scheelite Ore",
      "Sheldonite Ore",
      "Molybdenite Ore",
      "Tungstate Ore",
      "Titanium Dust",
      "Gallium Dust",
      "Stainless Steel Dust",
      "EV Circuit",
      "IV Circuit",
      "1k ME Storage Component",
      "1k ME Fluid Storage Component",
      "Glowstone",
      "Ender Pearl",
      "MV Machine Hull",
      "Iridium Ore",
      "HV Machine Hull",
      "Blank Pattern",
      "4k ME Storage Component",
      "4k ME Fluid Storage Component",
      "ME Interface",
      "Arsenic Dust",
      "Lutetium Dust",
      "Magnetite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "EV Machine Hull",
        "min": "1",
        "max": "1",
        "weight": 1,
        "globalWeight": 8,
        "percent": 3.27
      },
      {
        "name": "16k ME Storage Component",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 16,
        "percent": 6.53
      },
      {
        "name": "16k ME Fluid Storage Component",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 16,
        "percent": 6.53
      },
      {
        "name": "Naquadah Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 24,
        "percent": 9.8
      },
      {
        "name": "Osmium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 8,
        "percent": 3.27
      },
      {
        "name": "Indium Dust",
        "min": "1",
        "max": "2",
        "weight": 1,
        "globalWeight": 8,
        "percent": 3.27
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain unique ores or very good items",
    "textColor": "$GOLD",
    "minItemsPerOpen": "5",
    "maxItemsPerOpen": "5",
    "maxWeightReducer": 8
  },
  {
    "name": "lootbagCommon",
    "id": 5,
    "isActive": false,
    "includeDrop": [],
    "listOfIncludedItems": [],
    "usedItemNames": [
      "Oak Wood",
      "Apple",
      "Lignite Coal"
    ],
    "items": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 50
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 33.33
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 16.67
      }
    ],
    "extendableBag": false,
    "subTitle": "Primitive materials",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#3c3c3c",
    "isCustom": true,
    "maxWeightReducer": 1
  },
  {
    "name": "lootbagCommon",
    "id": 6,
    "isActive": false,
    "includeDrop": [],
    "listOfIncludedItems": [],
    "usedItemNames": [
      "Gravel",
      "Clay (ball)"
    ],
    "items": [
      {
        "name": "Clay (ball)",
        "min": "4",
        "max": "8",
        "weight": 1,
        "globalWeight": 1,
        "percent": 100
      }
    ],
    "extendableBag": false,
    "subTitle": "Building materials (virt)",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "maxWeightReducer": 1
  },
  {
    "name": "lootbagCommon",
    "id": 7,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "5",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 30,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 20,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 2
      }
    ],
    "usedItemNames": [
      "Magnetite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal"
    ],
    "items": [
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 40
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain iron",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#724748",
    "isCustom": true,
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagCommon",
    "id": 8,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "5",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 30,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 20,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 2
      }
    ],
    "usedItemNames": [
      "Chalcopyrite Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal"
    ],
    "items": [
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 40
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain copper",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#e26021",
    "isCustom": true,
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagCommon",
    "id": 9,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "5",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 30,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 20,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 2
      }
    ],
    "usedItemNames": [
      "Tin Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal"
    ],
    "items": [
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 4,
        "percent": 40
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain tin",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#bebebe",
    "isCustom": true,
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagCommon",
    "id": 10,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "5",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 30,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 20,
        "isIncluded": true,
        "increaseDropItems": 2
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 2
      }
    ],
    "usedItemNames": [
      "Redstone Ore",
      "Oak Wood",
      "Apple",
      "Lignite Coal"
    ],
    "items": [
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 4,
        "percent": 40
      }
    ],
    "extendableBag": true,
    "subTitle": "May contain redstone",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#cf1216",
    "isCustom": true,
    "maxWeightReducer": 2
  },
  {
    "name": "lootbagCommon",
    "id": 11,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 2,
        "bagId": "10",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 3,
        "reduceDropWeight": 6,
        "bagId": "5",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 2,
        "reduceDropWeight": 4,
        "bagId": 6,
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Oak Wood",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 3,
        "percent": 7.89,
        "isIncluded": true,
        "increaseDropItems": 3,
        "meetSpotCount": 126,
        "countOfGotItemsTotal": 1157,
        "itemGotCountFirstTry": 80,
        "itemsPerMeet": 9.18,
        "itemsPerOpen": 1.16,
        "simPercent": 12.66,
        "simPercentIfAlone": 8.04
      },
      {
        "name": "Apple",
        "min": "1",
        "max": "3",
        "weight": 2,
        "globalWeight": 2,
        "percent": 5.26,
        "isIncluded": true,
        "increaseDropItems": 3,
        "meetSpotCount": 88,
        "countOfGotItemsTotal": 537,
        "itemGotCountFirstTry": 57,
        "itemsPerMeet": 6.1,
        "itemsPerOpen": 0.54,
        "simPercent": 8.8,
        "simPercentIfAlone": 5.7
      },
      {
        "name": "Lignite Coal",
        "min": "2",
        "max": "4",
        "weight": 1,
        "globalWeight": 1,
        "percent": 2.63,
        "isIncluded": true,
        "increaseDropItems": 3,
        "meetSpotCount": 30,
        "countOfGotItemsTotal": 263,
        "itemGotCountFirstTry": 20,
        "itemsPerMeet": 8.77,
        "itemsPerOpen": 0.27,
        "simPercent": 3.1,
        "simPercentIfAlone": 2.11
      },
      {
        "name": "Clay (ball)",
        "min": "4",
        "max": "8",
        "weight": 1,
        "globalWeight": 2,
        "percent": 5.26,
        "isIncluded": true,
        "increaseDropItems": 2,
        "meetSpotCount": 59,
        "countOfGotItemsTotal": 706,
        "itemGotCountFirstTry": 41,
        "itemsPerMeet": 11.97,
        "itemsPerOpen": 0.73,
        "simPercent": 6.08,
        "simPercentIfAlone": 4.22
      },
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 15.79,
        "isIncluded": true,
        "increaseDropItems": 2,
        "meetSpotCount": 238,
        "countOfGotItemsTotal": 705,
        "itemGotCountFirstTry": 159,
        "itemsPerMeet": 2.96,
        "itemsPerOpen": 0.71,
        "simPercent": 23.87,
        "simPercentIfAlone": 15.96
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 15.79,
        "isIncluded": true,
        "increaseDropItems": 2,
        "meetSpotCount": 237,
        "countOfGotItemsTotal": 700,
        "itemGotCountFirstTry": 150,
        "itemsPerMeet": 2.95,
        "itemsPerOpen": 0.7,
        "simPercent": 23.7,
        "simPercentIfAlone": 15.12
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 6,
        "percent": 15.79,
        "isIncluded": true,
        "increaseDropItems": 2,
        "meetSpotCount": 244,
        "countOfGotItemsTotal": 727,
        "itemGotCountFirstTry": 175,
        "itemsPerMeet": 2.98,
        "itemsPerOpen": 0.73,
        "simPercent": 24.42,
        "simPercentIfAlone": 17.52
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 6,
        "percent": 15.79,
        "isIncluded": true,
        "increaseDropItems": 2,
        "meetSpotCount": 254,
        "countOfGotItemsTotal": 508,
        "itemGotCountFirstTry": 170,
        "itemsPerMeet": 2,
        "itemsPerOpen": 0.51,
        "simPercent": 25.5,
        "simPercentIfAlone": 17.1
      }
    ],
    "usedItemNames": [
      "Lignite Coal Block",
      "Oak Wood",
      "Apple",
      "Lignite Coal",
      "Sand",
      "Cobblestone",
      "Gravel",
      "Clay (ball)",
      "Magnetite Ore",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Lignite Coal Block",
        "min": "2",
        "max": "3",
        "weight": 1,
        "globalWeight": 6,
        "percent": 15.79,
        "meetSpotCount": 228,
        "countOfGotItemsTotal": 577,
        "itemGotCountFirstTry": 147,
        "itemsPerMeet": 2.53,
        "itemsPerOpen": 0.58,
        "simPercent": 22.96,
        "simPercentIfAlone": 14.8
      }
    ],
    "extendableBag": true,
    "subTitle": "LV bag (ores, fuel, building materials)",
    "textColor": "$WHITE",
    "minItemsPerOpen": "1",
    "maxItemsPerOpen": "2",
    "textureColor": "#718d41",
    "isCustom": true,
    "maxWeightReducer": 6
  },
  {
    "name": "lootbagCommon",
    "id": 12,
    "isActive": false,
    "includeDrop": [
      {
        "increaseDropItems": 3,
        "reduceDropWeight": 3,
        "bagId": "7",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 3,
        "reduceDropWeight": 3,
        "bagId": "8",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 3,
        "reduceDropWeight": 3,
        "bagId": "9",
        "listOfExtendedItems": []
      },
      {
        "increaseDropItems": 3,
        "reduceDropWeight": 3,
        "bagId": "10",
        "listOfExtendedItems": []
      }
    ],
    "listOfIncludedItems": [
      {
        "name": "Magnetite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 3
      },
      {
        "name": "Chalcopyrite Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 3
      },
      {
        "name": "Tin Ore",
        "min": "1",
        "max": "2",
        "weight": 2,
        "globalWeight": 2,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 3
      },
      {
        "name": "Redstone Ore",
        "min": "1",
        "max": "1",
        "weight": 2,
        "globalWeight": 2,
        "percent": 10,
        "isIncluded": true,
        "increaseDropItems": 3
      }
    ],
    "usedItemNames": [
      "Coke Coal Block",
      "Aluminium Ore",
      "Sand",
      "Cobblestone",
      "Gravel",
      "Clay (ball)",
      "Magnetite Ore",
      "Chalcopyrite Ore",
      "Tin Ore",
      "Redstone Ore"
    ],
    "items": [
      {
        "name": "Coke Coal Block",
        "min": "2",
        "max": "3",
        "weight": 1,
        "globalWeight": 3,
        "percent": 15
      },
      {
        "name": "Aluminium Ore",
        "min": "2",
        "max": "4",
        "weight": 3,
        "globalWeight": 9,
        "percent": 45
      }
    ],
    "extendableBag": true,
    "subTitle": "MV bag (ores, fuel, building materials)",
    "textColor": "$WHITE",
    "minItemsPerOpen": 2,
    "maxItemsPerOpen": 2,
    "textureColor": "#4ebdcd",
    "isCustom": true,
    "maxWeightReducer": 3
  }
];