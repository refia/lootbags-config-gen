Vue.component('translate',
    {
        props: ['text', 'lang'],
        data: function () {
            return {
                translatedText: ''
            };
        },
        watch: {
            lang: function(newVal, oldVal) {
                this.doTranslate();
            },
            text: function (newVal, oldVal) {
                this.doTranslate();
            }
        },
        methods: {
            doTranslate: function () {
                this.translatedText = (this.text || '').trim().replace(/\s+/g,' ');
                if(translateMap[this.lang] && translateMap[this.lang][this.translatedText]){
                    this.translatedText = translateMap[this.lang][this.translatedText];
                }
            }
        },
        mounted: function(){
            this.doTranslate();
        },
        template: '<span>{{translatedText}}</span>'
    }
);