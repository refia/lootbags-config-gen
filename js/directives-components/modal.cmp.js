Vue.component('modal',
    {
        props: ['params', 'lang'],
        data: function () {
            return {};
        },
        methods: {
        },
        mounted: function () {
        },
        template: `
            <div class="modal">
                <div class="modalBody" v-bind:style="params.bodyStyle">
                    <div class="btnSet bb">
                        <button v-for="btn in params.buttons" @click="btn.action()" :class="btn.class || ''">
                            <span><translate :lang="lang" :text="btn.name"></translate></span>
                        </button>
                    </div>
                    <div>
                        <slot></slot>
                    </div>
                </div>
            </div>
        `
    }
);