Vue.component('select-search',
    {
        props: ['value', 'list', 'exceptions'],
        data: function () {
            return {
                isDropOpen: false,
                filteredList: [],
                oldVal: '',
                selectedVal: ''
            };
        },
        mounted: function(){
            this.oldVal = this.selectedVal = this.value;
        },
        methods: {
            showSearchPopup: function (textVal) {
                this.filteredList = [];
                this.list.forEach(i =>{
                    if(!this.exceptions || this.exceptions.length < 1 || this.exceptions.indexOf(i) < 0){
                        if(!textVal) {
                            this.filteredList.push(i);
                        } else {
                            if(i.toLowerCase().indexOf(textVal.toLowerCase()) > -1){
                                this.filteredList.push(i);
                            }
                        }
                    }
                });
                this.isDropOpen = true;
            },
            onItemSelected: function (fItem) {
                this.oldVal = this.selectedVal;
                this.selectedVal = fItem;
                this.isDropOpen = false;
                this.$emit('valselect', {oldVal: this.oldVal, newVal: this.selectedVal});
                this.$emit('input', this.selectedVal);
            },
            doOnBlur: function(){
                setTimeout(()=>{
                    this.isDropOpen=false;
                    if(this.selectedVal != this.value){
                        this.oldVal = this.selectedVal;
                        this.selectedVal = '';
                        this.$emit('valselect', {oldVal: this.oldVal, newVal: this.selectedVal});
                        this.$emit('input', '');
                    }
                }, 200);
            }
        },
        template: `
<span class="select-search">
    <input v-bind:value="value" v-on:focus="showSearchPopup(value)" v-on:blur="doOnBlur"
    v-on:input="$emit('input', $event.target.value);showSearchPopup($event.target.value)" >
    <br/>
    <span class="selectContainer" v-if="isDropOpen">
            <span v-for="fItem in filteredList" @click="onItemSelected(fItem)"
            class="itemToSelect">
            {{fItem}}
            </span><br/>
    </span>
</span>
  `
    }
);